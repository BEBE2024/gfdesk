# 工分标准（V20241011）
- 工分获取标准：20,000,000工分/天贡献量（至**2025年6月30日**）
- 具体贡献基于工时进行评价核算（详见**工分公示**）

# 
# 工分公示

说明：
- 共建人针对于自然人
- 工分是共建人对项目贡献多少的量化评价，是共建项目分配项目收益的依据
- 工分证明是发放工分所对应的具体贡献的证明
- 当某工分证明含有多个共建人贡献成果、而又难以清晰分割时，该工分证明所对应工分的分配比例，以相关共建人共识确认为准


**项目维度**
| BEBE社区 |  |  |
|--|--|--|
| 晨冲冲 | 工分（工时） | [工分证明](https://blog.csdn.net/weixin_42249843/article/details/142849722?sharetype=blogdetail&sharerId=142849722&sharerefer=PC&sharesource=weixin_42249843&spm=1011.2480.3001.8118) |

| 一起学光机 |  |  |
|--|--|--|
| 晨冲冲 | 620,000,000 BEBE工分（31天） | [工分证明](https://blog.csdn.net/weixin_42249843/article/details/142849722?sharetype=blogdetail&sharerId=142849722&sharerefer=PC&sharesource=weixin_42249843&spm=1011.2480.3001.8118)  |

| 科 学 观 察 |  |  |
|--|--|--|
| 晨冲冲 | 300,000,000 BEBE工分（15天） | [工分证明](https://blog.csdn.net/weixin_42249843/article/details/142849722?sharetype=blogdetail&sharerId=142849722&sharerefer=PC&sharesource=weixin_42249843&spm=1011.2480.3001.8118) |

| 乐 符 跳 动 |  |  |
|--|--|--|
| 晨冲冲 | 980,000,000 BEBE工分（49天） | [工分证明](https://blog.csdn.net/weixin_42249843/article/details/142849722?sharetype=blogdetail&sharerId=142849722&sharerefer=PC&sharesource=weixin_42249843&spm=1011.2480.3001.8118) |

| 职 业 观 察 |  |  |
|--|--|--|
| 晨冲冲 | 40,000,000 BEBE工分（2天） | [工分证明](https://blog.csdn.net/weixin_42249843/article/details/142849722?sharetype=blogdetail&sharerId=142849722&sharerefer=PC&sharesource=weixin_42249843&spm=1011.2480.3001.8118) |

| 正气合规组 |  |  |
|--|--|--|
| 某某某 | 工分（工时） | 工分证明 |

# 
**共建人维度**
| 共建成员 | 获得工分 | 工分证明 |
|--|--|--|
| 晨冲冲 |  | [个人主页](https://blog.csdn.net/weixin_42249843/article/details/142849722?sharetype=blogdetail&sharerId=142849722&sharerefer=PC&sharesource=weixin_42249843&sharefrom=mp_from_link)|
|  |  |  |